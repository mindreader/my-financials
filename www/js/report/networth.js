$(document).ready(function() {
  $(".dateInput").datepicker({dateFormat:'yy-mm-dd'});
  drawNetworthChart();

  $('.timePeriod').click(function() { 
    updateTimePeriod(this.id);
  });
});

$(window).resize(function() { drawNetworthChart(); });


function redrawChart() {
  drawNetworthChart();
}

function updateDateRange() {
  drawNetworthChart();
}

		       
function drawNetworthChart() {  
  var dataToPlot = [];

  var from = $('#from').val();
  var fromTS = new Date(from).getTime();
  var to = $('#to').val();
  var toTS = new Date(to+' 23:59:59').getTime();


  var previousPointAdded = false;
  var lastPointAdded = -1;
  for (var i = 0; i < window.networthLog.length; i++) {
    var wl = window.networthLog[i];

    if ((wl[0] > fromTS) && (wl[0] < toTS)) {
      if (!previousPointAdded && (i > 0)) {
        var previousPt = [];
        previousPt[0] = window.networthLog[i-1][0];
        previousPt[1] = window.networthLog[i-1][1];
	dataToPlot.push(previousPt);
        previousPointAdded = true;
      }
      dataToPlot.push(wl);
      lastPointAdded = i;
    }
  }
  if (lastPointAdded < (window.networthLog.length-1)) {
    var pointAfter = [];
    pointAfter[0] = window.networthLog[lastPointAdded+1][0];
    pointAfter[1] = window.networthLog[lastPointAdded+1][1];
    dataToPlot.push(pointAfter);
  }
  

  var now = new Date();

  var rangeStart = fromTS;
  var rangeEnd = toTS;

  var options = {
    grid: { hoverable:true },
    xaxis: {
      mode: "time",
      min: rangeStart,
      max: rangeEnd,
      timezone: "browser"
    },
    tooltip: true,
    tooltipOpts: {	
      content: function(label, xval, yval) { return formatTimeLabel(xval) + " - $" + yval; },
      shifts: { x:-40, y: 25 }
    }
  };
  console.log(options);
  console.log(dataToPlot);
  $('#fullScreenChart').html('');
  $.plot("#fullScreenChart", [dataToPlot], options);
}