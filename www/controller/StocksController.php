<?php

namespace ToddHudgens\MyFinancials\Controller;

use ToddHudgens\MyFinancials\model\Stocks as Stocks;
use ToddHudgens\MyFinancials\model\Twig as Twig;
use PDO;

class StocksController { 

function updatePrices() {
  $dbh = dbHandle();

  // get all of the unique stocks we need to track
  $q = 'SELECT distinct ticker FROM stockAssets WHERE qty<>0';
  $results = $dbh->query($q);
  $stockTickers = $results->fetchAll(PDO::FETCH_ASSOC);
  //print_r($stockTickers);

  // update their prices
  foreach ($stockTickers as $i => $info) {
    echo "Updating price for: " . $info['ticker'];
    $response = Stocks::updatePrice($info['ticker']);
    if ($response) { echo " (success)<br>"; } 
    else { echo " (failure)<br>"; }
  }

  // update the stockAssets table
  foreach ($stockTickers as $i => $info) { 
    $price = Stocks::getLatestPrice($info['ticker']);
    Stocks::updateAssetsWithTicker($info['ticker'], $price);
  }
}

}

?>