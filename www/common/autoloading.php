<?php

spl_autoload_register('my_autoloader');

// setup autoloading of classes
function my_autoloader($class) {

  // project-specific namespace prefix
  $prefix = 'ToddHudgens\\MyFinancials\\';

  // base directory for the namespace prefix
  $base_dir = __DIR__ . '/../';

  // does the class use the namespace prefix?
  $len = strlen($prefix);
  if (strncmp($prefix, $class, $len) !== 0) {
    // no, move to the next registered autoloader
    return;
  }

  // get the relative class name
  $relative_class = substr($class, $len);

  // replace the namespace prefix with the base directory, replace namespace
  // separators with directory separators in the relative class name, append
  // with .php
  $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

  // if the file exists, require it
  if (file_exists($file)) {
    require $file;
  }
}



function autoloadCSS() {
  global $css;
  $fn = strtolower($GLOBALS['controller']).'.css';
  if (isset($GLOBALS['controller']) && file_exists("css/".$fn) && !in_array($fn, array_values($css))) {
    $css[] = $fn; 
  }

  $fn = strtolower($GLOBALS['controller']).'/'.strtolower($GLOBALS['action']).'.css';
  if (isset($GLOBALS['controller']) && 
      isset($GLOBALS['action']) && 
      file_exists("css/".$fn) && 
      !in_array($fn, array_values($css))) {
    $css[] = $fn;
  }
}


function autoloadJS() {
  global $js;
  $fn = strtolower($GLOBALS['controller']).'.js';
  if (isset($GLOBALS['controller']) && 
      file_exists( "js/".strtolower($GLOBALS['controller']).".js") && 
      !in_array($fn, array_values($js))) {
    $js[] = $fn;
  }

  $fn = strtolower($GLOBALS['controller']).'/'.strtolower($GLOBALS['action']).'.js';
  if (isset($GLOBALS['controller']) && 
      isset($GLOBALS['action']) &&
      file_exists("js/".$fn) && 
      !in_array($fn, array_values($js))) {
    $js[] = $fn;
  }
}

?>