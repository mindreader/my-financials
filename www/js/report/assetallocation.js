$(document).ready(function() { plotAssetAllocation(); });
$(window).resize(function() { plotAssetAllocation(); });

function plotAssetAllocation() {   
  var options = {
    grid: {
      hoverable: true,
      clickable: true 
    },
    series: {
      pie: { show: true }
    }
  };
  $.plot($("#fullScreenChart"), window.assetAllocationData, options);
}