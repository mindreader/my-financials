<?php

namespace ToddHudgens\MyFinancials\Model;

use ToddHudgens\MyFinancials\model\Category as Category;


class Grocery extends AbstractPlugin { 

  public static $foodCategories = array('Food', 'Groceries', 'Dining Out', 'Beer', 
                                        'Coffee', 'Cheese', 'Honey', 'Wine', 'Liquor');

  public static function transactionCreate($transactionId) {
    self::transactionUpdate($transactionId);
  }

  public static function transactionUpdate($transactionId) {
    $categoryIds = Category::getSubmittedCategories();

    // is this a matching transaction?
    $matchingTransaction = false;
    foreach ($categoryIds as $i => $categoryId) { 
      if (in_array(Category::getName($categoryId), self::$foodCategories)) { 
        $matchingTransaction = true; 
      }
    }

    $apiEndpoint = getenv('GROCERY_TRIP_API_ENDPOINT');

    // if so, make the API request to my food and diet tracker
    if ($matchingTransaction && ($apiEndpoint != "")) { 
      $apiEndpoint = getenv('GROCERY_TRIP_API_ENDPOINT');
      $requestURI = $apiEndpoint;
      $requestURI .= '?transactionId='.$transactionId;
      $requestURI .= '&userId=1';
      $requestURI .= '&date='.$_REQUEST['date'];
      $requestURI .= '&payee='.urlencode($_REQUEST['payeeName']);
      $requestURI .= '&total='.$_REQUEST['total'];
      $requestURI .= '&notes='.urlencode($_REQUEST['notes']);

      file_get_contents($requestURI);
    }
  }

  
}


?>