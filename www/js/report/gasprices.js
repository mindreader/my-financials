$(document).ready(function() {
  $(".dateInput").datepicker({dateFormat:'yy-mm-dd'});
  drawGasPriceChart();

  $('.timePeriod').click(function() {
    updateTimePeriod(this.id);
  });

});

$(window).resize(function() { drawGasPriceChart(); });


function updateDateRange() {
  drawGasPriceChart();
}

function redrawChart() {
  drawGasPriceChart();
}
		       
function drawGasPriceChart() {  
  var dataToPlot = [];

  var from = $('#from').val();
  var fromTS = new Date(from).getTime();
  var to = $('#to').val();
  var toTS = new Date(to).getTime();

  var previousPointAdded = false; 
  var lastPointAdded = -1;
  for (var i = 0; i < window.gasPriceLog.length; i++) {
    var wl = window.gasPriceLog[i];

    if ((wl[0] > fromTS) && (wl[0] < toTS)) {
      if (!previousPointAdded && (i > 0)) {	  
        var previousPt = [];
        previousPt[0] = window.gasPriceLog[i-1][0];
        previousPt[1] = window.gasPriceLog[i-1][1];
        dataToPlot.push(previousPt);
        previousPointAdded = true;
      }
      dataToPlot.push(wl);
      lastPointAdded = i;
    }
  }
  if (lastPointAdded < (window.gasPriceLog.length-1)) {   
    var pointAfter = [];
    pointAfter[0] = window.gasPriceLog[lastPointAdded+1][0];
    pointAfter[1] = window.gasPriceLog[lastPointAdded+1][1];
    dataToPlot.push(pointAfter);
  }

  var now = new Date();
  var rangeStart = fromTS;
  var rangeEnd = toTS;

  var options = {
    xaxis: {
      mode: "time",
      min: rangeStart,
      max: rangeEnd,
      timezone: "browser"
    },
    grid: { hoverable:true },
    tooltip: true,
    tooltipOpts: {	   
      content: function(label, xval, yval) { return formatTimeLabel(xval) + " - $" + yval; }, 
      shifts: { x:-40, y: 25 } 
    }
  };
  $('#fullScreenChart').html('');
  $.plot("#fullScreenChart", [dataToPlot], options);
}