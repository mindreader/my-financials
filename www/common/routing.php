<?php

function routeRequest() {
 
  $uri = $_SERVER['REQUEST_URI'];

  // process query string, put into $_GET array
  $qMarkPos = strpos($uri, "?");
  $qs = '';
  if (is_int($qMarkPos)) { 
    $qs = substr($uri, $qMarkPos+1);
    $uri = substr($uri, 0, $qMarkPos);
  }

  try { 
    $dbh = dbHandle();
    $stmt = $dbh->prepare('SELECT * FROM pages WHERE uri=?');
    $stmt->execute(array($uri));
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $GLOBALS['controller'] = ''; 
    $GLOBALS['action'] = ''; 
    $GLOBALS['applyView'] = ''; 
    $GLOBALS['queryString'] = ''; 

    if (count($results) > 0) {  
      $row = $results[0];

      $GLOBALS['pageId'] = $row['id'];

      if ($row['controller'] != "") { 
        $GLOBALS['controller'] = $row['controller'];
        $GLOBALS['action'] = $row['action']; 
        $GLOBALS['title'] = $row['title'];
        $GLOBALS['applyView'] = $row['applyView'];
        $GLOBALS['loginRequired'] = $row['loginRequired']; 

        foreach ($_GET as $key => $value) { 
          if ($key != "path") { 
            $GLOBALS['queryString'] .= $key . '=' . $value . '&'; 
          }
        }

        if ($row['queryString'] != "") { 
	 $GLOBALS['queryString'] .= $row['queryString'];
         parse_str($GLOBALS['queryString'], $_GET);
        }
        return;
      }
    }
  }
  catch (PDOException $e) {
    die($e->getMessage());
  }
}


?>